v0.1.27 added consumable deletion on last item use.  
fix for damage application when more 1 GM logged in. Should fix people having problems with traps  
added Hooks MinorQolDamageApplied so that traps can know when to restore targets.  
v0.1.26 Added ability to display item cards when doing speed rolls.  
The item card is displayed based on the new speedItemRolls settings of , Off|On|On + ItemCards. The module will display the actionbuttons on the card for actions that are not speed rolled.  
CLeaned up another edgee case for consumable consumption  
v0.1.25 Cleaned up resource/charge consumption in speed iteem rolls.  
v0.1.24 Fixed processing betterrolls saving throws when players do their own saves and LMRTFY not installed.  
cleaned up timeout handling for player saves  
support right click on charater sheet to do versatile attack  
corrected error on critical threshold in better rolls  
Updated Readme.md to be more current.  
v0.1.23 Correct application of effects for better rolls  
v0.1.22 support itemId in minorQOL.doRoll() as well as item name.  
v0.1.21 Added "no damage on save" text when parsing description for spells/items that do no damage on save.  
v0.1.20 fixed better rolls support:  
correctly determine hit/miss/critical on single/dual rolls with normal/advantage/disadvantage.  
v0.1.18 Fix many edge cases for player rolled saving throws for measured templates that target self.  
v0.1.17 Fix so that gmroll/blind roll is always respected.  
Fix to self targetting so preferentially self target, rather than self target when no other targets selected.  
v0.1.14 add AttackRollFlavorTextAlt, DamageRollFlavorTextAll, SavingThrowTextAlt so that you can have localized and non-localized versions of those strings if required.  
v0.1.13 1/2 damage on saves can now be configured to require the text "half as much dmage" or "half damage" to appear in the description as well as specifying a saving throw. Congure from module settings damage immunities.  
You can now use MESS templates for placing templates with minor-qol. The display will be slightly strange as MESS tempaltes targets all tokens during placement. minor-qol will then filter the targeted tokens on placement.  
In conjunction with dynamiceffects a new flag flags.dnd5e.attackAdvantage is supported for auto rolled attacks. -1 = disadvantage +1 = advantage. Applies to all rolled attacks.  
In conjunction with dynamiceffects a new flag forceCritical for auto rolled damage rolls. True => make any hit a critical.  
Minor-qol roll-damage button no longer keeps focus.  
Reverse damage cards now display all dr/di/dv fields to help the dm confirm the applied damage is correct.  
v0.1.12 Fixed behaviour of autoSaves on autoRoll damage off when roll damage button is pressed.  
Display spell cast dialog when casting via "standard roll button"  
fix for innacte spell casting consumption when charges = 1  
added option to always display saving throws.  
update ja.jason  
v0.1.07 fix for not parsing damage rolls correctly  
v0.1.05 +added untarget feature (module config). At the end of your turn untarget none, dead or all tokens. Requested a few times.  
added support for tidy5eSheet  
minor-qol now uses the default dnd5e spell cast dialog so you can choose not to consume a slot if you wish.  
added support for better rolls. This is an alpha release so there will be bugs.  
To use minor-qol with better rolls you should disable "speed rolls" and "add item sheet buttons" in minor-qol.  
You will need to set better rolls to only roll a single dice for attack rolls or it will break.  
All of the other minor-qol flags flags are supposed to work, auto-check hits, saves, apply damage etc.  
Damage immunities/resitance should work, but do check.  
Auto application of dynamiceffects effects is supported (disable via the minor-qol flag if you don't want that).  
When you find bugs, please try and give as much information as you can to help me work out what is going on.   
v 0.1.03 hopefully finally fixed the consumption to match the dnd5e standard  
0.1.01 v0.9 DND5e compatibility. Chages to item charge usage/consumption in line with new dnd 0.9 behaviour. This version REQUIRES DND 0.9   
0.1.00 fixed self targeting so that actions that have a target of self preferntially apply to self over the selected targets  
fixed damage/healing application when consuming the last potion of a given type.  
added advantage display for saving throws made with magic resistance.  
Change kr.json to ko.json  
0.96 Fix for concentration and saving throws for CUB  
0.95 Another go at hiding the above chat cards from the user.  
Changed autoRollDamage to allow always rolling damage even if attack misses.  
<BREAKING> Chnaged chatcard damage buttons so that apply damage ALWAYS reduces hit points, apply healing ALWAYS increases hit points.  
0.94 Changed autoCheckHit/autoCheckSave option to allow showing the chat card only to GM.  
0.93 fix for cantrips not working  
0.92 Chat damage buttons not working  
Casting spells with auto-target off causes the spell to not complete.  
0.90 fix for spell dc override, spells finally use slots correctly, template effects with a range of self do not target the caster.  
0.89 fixed typo in above fix.  
0.88 added compatibility for new spell slot features including pact magic.  
0.0.86 Fixed spell cast dialog to support pact spells  
Fixed problem with charge consumption for feats. 
0.0.85 add option to apply damage but don't display undo damage card.  
0.0.84 reinstate support for roll buttons on betternpcsheet.  
0.0.83 add support for DNDBeyondSheet5e for speed item rolls.  
0.0.82 fix bug with item buttons being added multiple times.  
If auto roll damage is disabled and speed item roll is enabled a roll damage button is added to the attack roll card.  