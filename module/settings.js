"use strict";
import {fetchParams} from "./minor-qol.js"
export const MODULE_NAME = 'minor-qol';

/**
 * The module title
 */
export const title = "Minor QOL settings";

/**
 * Some generic path references that might be useful later in the application's windows
 */
export const path = {
  root: `modules/${MODULE_NAME}/`,
  rollTemplate: `modules/${MODULE_NAME}/templates/roll.html`
};

/**
 * For each setting, there is are two corresponding entries in the language file to retrieve the translations for
 * - the setting name
 * - the hint displayed beneath the setting's name in the "Configure Game Settings" dialog.
 *
 * Given your MODULE_NAME is 'my-module' and your setting's name is 'EnableCritsOnly', then you will need to create to language entries:
 * {
 *  "my-module.EnableCritsOnly.Name": "Enable critical hits only",
 *  "my-module.EnableCritsOnly.Hint": "Players will only hit if they crit, and otherwise miss automatically *manic laughter*"
 * }
 *
 * The naming scheme is:
 * {
 *  "[MODULE_NAME].[SETTING_NAME].Name": "[TEXT]",
 *  "[MODULE_NAME].[SETTING_NAME].Hint": "[TEXT]"
 * }
 */
const settings = [

  {
    name: "ItemRollButtons",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "SpeedItemRolls",
    default: "Off",
    type: String,
    choices: {off: "Off", on: "On", onCard: "On + Show Item Card"},
    onChange: (value) => {window.location.reload()}
  },
  {
    name: "AutoTarget",
    scope: "world",
    default: "None",
    type: String,
    choices: {none: "None", always: "Always", wallsBlock: "Walls Block"},
    onChange: fetchParams
  },
  {
    name: "AutoCheckHit",
    scope: "world",
    choices: {none: "None", all: "Check - all see result", whisper: "Check - only GM sees"},
    default: "none",
    type: String,
    onChange: fetchParams
  },
  {
    name: "AutoCheckSaves",
    scope: "world",
    choices: {none: "None", all:  "Save - All see result", whisper: "Save - only GM sees", allShow: "Save - All see Result + Rolls"},
    default: "all",
    type: String,
    onChange: fetchParams
  },
  {
    name: "PlayerRollSaves",
    scope: "world",
    choices: {none: "None",  letme: "Let Me Roll That For You", chat: "Chat Message"},
    default: "none",
    type: String,
    onChange: fetchParams
  },
  {
    name: "PlayerSaveTimeout",
    scope: "world",
    default: 30,
    type: Number,
    config: true,
    onChange: fetchParams
  },
  {
    name: "AutoRollDamage",
    scope: "world",
    choices: {none: "None", always:  "Always", onHit: "Attack Hits"},
    default: "onHit",
    type: String,
    onChange: fetchParams
  },
  {
    name: "AddChatDamageButtons",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "AutoApplyDamage",
    scope: "world",
    default: "yesCard",
    type: String,
    choices: {none: "No", yes: "Yes", yesCard: "Yes + undo damage card"},
    onChange: fetchParams
  },
  {
    name: "AutoRemoveTargets",
    scope: "world",
    default: "dead",
    type: String,
    choices: {none: "Never", dead: "untarget dead", all: "untarget all"},
    onChange: fetchParams
  },
  {
    name: "DamageImmunities",
    scope: "world",
    default: "savesDefault",
    type: String,
    choices: {none: "Never", savesDefault: "Apply saves - no text check", savesCheck: "Apply Saves - check text"},
    onChange: fetchParams
  },
  {
    name: "AutoEffects",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "MacroSpeedRolls",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "HideNPCNames",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "ItemDeleteCheck",
    scope: "client",
    default: true,
    type: Boolean,
    choices: [],
    onChange: fetchParams
  },
  {
    name: "showGM",
    scope: "world",
    default: false,
    type: Boolean,
    choices: [],
    onChange: fetchParams
  },
  {
    name: "ColoredBorders",
    scope: "world",
    default: "None",
    type: String,
    choices: {none: "None", borders: "Borders Only", borderNames: "Border + Name"},
    onChange: fetchParams
  },
  {
    name: "RangeTarget",
    scope: "world",
    default: false,
    type: Boolean,
    choices: [],
    onChange: fetchParams
  }
  
]

export const registerSettings = () => {
  settings.forEach(setting => {
    let options = {
        name: game.i18n.localize(`${MODULE_NAME}.${setting.name}.Name`),
        hint: game.i18n.localize(`${MODULE_NAME}.${setting.name}.Hint`),
        scope: setting.scope,
        config: true,
        default: setting.default,
        type: setting.type,
        onChange: setting.onChange
    };
    if (setting.choices) options.choices = setting.choices;
    game.settings.register(MODULE_NAME, setting.name, options);
  });
}

